const util = require('../../utils/util.js')

Page({
  data: {
    showTopTips: false,
    date: util.nowDate(),
    time: util.nowTime(),
    seatCounts: [1, 2, 3, 4, 5, 6],
    seatCountIndex: 0,
    departure: '',
    destination: '',
    isAgree: true
  },
  onLoad: function (options) {
    this.setData({
      addType: options.addType
    })
    console.log(this.data.addType)
  },
  showTopTips: function () {
    var that = this;
    this.setData({
      showTopTips: true
    });
    setTimeout(function () {
      that.setData({
        showTopTips: false
      });
    }, 3000);
  },
  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },
  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value
    })
  },
  bindSeatChange: function (e) {
    console.log('picker seat 发生选择改变，携带值为', e.detail.value);
    this.setData({
      seatCountIndex: e.detail.value
    })
  },
  chooseDeparture: function (e) {
    var that = this
    wx.chooseLocation({
      success: function (res) {
        console.log(res)
        that.setData({
          departure: res.name
        })
      }
    })
  },
  chooseDestination: function (e) {
    var that = this
    wx.chooseLocation({
      success: function (res) {
        console.log(res)
        that.setData({
          destination: res.name
        })
      }
    })
  }


});