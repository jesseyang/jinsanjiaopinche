const util = require('../../utils/util.js')

Page({
  data: {
    nickname: '',
    showTopTips: false,
    date: util.nowDate(),
    time: util.nowTime(),
    phoneNumber:'',
    seatCounts:[1,2,3,4,5,6],
    seatCountIndex:0,
    departure:'',
    departureDetail: '',
    destination:'',
    destinationDetail: '',
    isAgree: true
  },
  onLoad: function(options){
    this.setData({
      addType: options.addType,
      nickname: getApp().globalData.userInfo.nickName
    })   
    
  },
  showTopTips: function () {
    var that = this;
    this.setData({
      showTopTips: true
    });
    setTimeout(function () {
      that.setData({
        showTopTips: false
      });
    }, 3000);
  },
  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },
  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value
    })
  },
  bindSeatChange: function (e) {
    this.setData({
      seatCountIndex: e.detail.value
    })
  },
  chooseDeparture: function (e) {
    var that = this
    wx.chooseLocation({
      success: function (res) {
        console.log(res)
        that.setData({
          departure: res.name,
          departureDetail: res.address
        })
      }
    })
  },
  chooseDestination: function (e) {
    var that = this
    wx.chooseLocation({
      success: function (res) {
        console.log(res)
        that.setData({
          destination: res.name,
          destinationDetail: res.address
        })
      }
    })
  },
  phoneInput: function(e){
    this.setData({
      phoneNumber:e.detail.value
    })
  },
  nameInput: function (e) {
    this.setData({
      nickname: e.detail.value
    })
  },
  save: function(){
    var that = this
    wx.request({
      url: 'https://jesseyang.tunnel.qydev.com/lookForCar/saveLookForCar',
      data:{
        nickname:that.data.nickname,
        phone: that.data.phoneNumber,
        departureDate: that.data.date,
        departureTime: that.data.time,
        departure: that.data.departure,
        departureDetail: that.data.departureDetail,
        destination: that.data.destination,
        destinationDetail: that.data.destinationDetail,
        seat: that.data.seatCounts[that.data.seatCountIndex],
        openId: getApp().globalData.userInfo.openId
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success:function(){
        wx.redirectTo({
          url: '/pages/info/msgSuccess'
        })
      }
    })
  }


});