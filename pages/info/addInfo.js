const util = require('../../utils/util.js')

Page({
  data: {
    nickname: '',
    showTopTips: false,
    date: util.nowDate(),
    time: util.nowTime(),
    phoneNumber: '',
    seatCounts: [1, 2, 3, 4, 5, 6],
    seatCountIndex: 0,
    departure: '',
    departureDetail: '',
    destination: '',
    destinationDetail: '',
    isAgree: true,
    hasUserInfo:false,
  },
  onLoad: function (options) {
 //   console.log('userInfo:' + getApp().globalData.userInfo)
    this.setData({
      addType: options.addType,
      nickname: (getApp().globalData.userInfo != null) ? getApp().globalData.userInfo.nickName : '',
      hasUserInfo: (getApp().globalData.userInfo != null) ? true : false
    })

  },
  showTopTips: function () {
    var that = this;
    this.setData({
      showTopTips: true
    });
    setTimeout(function () {
      that.setData({
        showTopTips: false
      });
    }, 3000);
  },
  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },
  bindTimeChange: function (e) {
    this.setData({
      time: e.detail.value
    })
  },
  bindSeatChange: function (e) {
    this.setData({
      seatCountIndex: e.detail.value
    })
  },
  chooseDeparture: function (e) {
    var that = this
    wx.chooseLocation({
      success: function (res) {
        console.log(res)
        that.setData({
          departure: res.name,
          departureDetail: res.address
        })
      }
    })
  },
  chooseDestination: function (e) {
    var that = this
    wx.chooseLocation({
      success: function (res) {
        console.log(res)
        that.setData({
          destination: res.name,
          destinationDetail: res.address
        })
      }
    })
  },
  phoneInput: function (e) {
    this.setData({
      phoneNumber: e.detail.value
    })
  },
  nameInput: function (e) {
    this.setData({
      nickname: e.detail.value
    })
  },
  departureInput:function(e){
    this.setData({
      departure:e.detail.value
    })
  },
  destinationInput: function (e) {
    this.setData({
      destination: e.detail.value
    })
  },
  save: function () {
    var that = this
    wx.showModal({
      content: '发布前请确认已阅读并同意免责声明',
      showCancel: true,
      success: function (res) {
        if (res.confirm) {
          wx.setStorage({
            key: 'from',
            data: that.data.departure,
          })
          wx.setStorage({
            key: 'to',
            data: that.data.destination,
          })
          wx.request({
            url: 'https://api.hgtengfei.cn/publishedInfo/save',
            data: {
              nickname: that.data.nickname,
              phone: that.data.phoneNumber,
              departureDate: that.data.date,
              departureTime: that.data.time,
              departure: that.data.departure,
              departureDetail: that.data.departureDetail,
              destination: that.data.destination,
              destinationDetail: that.data.destinationDetail,
              seat: that.data.seatCounts[that.data.seatCountIndex],
              openId: getApp().globalData.userId,
              type: that.data.addType
            },
            header: {
              'content-type': 'application/json' // 默认值
            },
            success: function (data) {
              console.log(data)
              if (data.data.success) {
                wx.redirectTo({
                  url: '/pages/info/msgSuccess'
                })
              } else {
                wx.showModal({
                  content: data.data.msg,
                  showCancel: false,
                  success: function (res) {
                    if (res.confirm) {
                      console.log('用户点击确定')
                    }
                  }
                });
              }

            }
          })
        }
      }
    });
     
  },
  bindGetUserInfo:function(e){
    console.log(e.detail.userInfo)
  }


});