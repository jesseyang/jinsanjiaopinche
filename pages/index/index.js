var sliderWidth = 96; // 需要设置slider的宽度，用于计算中间位置
var currentPage = 1;
var pageSize = 10;
Page({
  data: {
    tabs: ["我是乘客，找车", "我是车主，找人"],
    activeIndex: 0,
    sliderOffset: 0,
    sliderLeft: 0,
    imgUrls: [
      '../../img/wspc_car.png',
      '../../img/qqyd.png',
      '../../img/yjjy.png',
    ],
    searchDeparture:'',
    searchDestination:'',
    interval: 5000,
    duration: 1000,
    nomore: false,
    pincheData:[],
    type:1
  },
  onLoad: function () {
    var that = this;
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          sliderLeft: (res.windowWidth / that.data.tabs.length - sliderWidth) / 2,
          sliderOffset: res.windowWidth / that.data.tabs.length * that.data.activeIndex
        });
      }
    });
   

  },
  onShow:function(){
    this.resetAll()
  },
  tabClick: function (e) {
    console.log(e.currentTarget.id)
    this.setData({
      sliderOffset: e.currentTarget.offsetLeft,
      activeIndex: e.currentTarget.id,
      type: parseInt(e.currentTarget.id) + 1
    });
    this.resetAll()
  },
  makeCall:function (e) {
    console.log(e)
    wx.makePhoneCall({
      phoneNumber: e.target.dataset.phone //仅为示例，并非真实的电话号码
    })
  },
  lookForCarPage: function (e) {
    wx.navigateTo({
      url: '/pages/info/addInfo?addType=1'
    })
  },
  lookForPeoplePage: function (e) {
    wx.navigateTo({
      url: '/pages/info/addInfo?addType=2'
    })
  },
  searchByDeparture: function (e){
    var that = this
    this.setData({
      searchDeparture: e.detail.value
    })
    console.log(that.data.searchDeparture)
  },
  searchByDestination: function (e) {
    var that = this
    this.setData({
      searchDestination: e.detail.value
    })
    console.log(that.data.searchDestination)
  },
  onPullDownRefresh: function () {
    currentPage = 1
    this.setData({
      pincheData:[],
      nomore:false
    })
    this.getList()
    
    wx.showToast({
      title: '刷新成功',//提示信息
      icon: 'success',//成功显示图标
      duration: 500//时间
    })
  },
  onReachBottom: function () {
    console.log('到底了')
    var tempData = this.data.pincheData
    currentPage ++
    this.getList()
  },
  clearSearch: function () {
    this.setData({
      searchDestination: '',
      searchDeparture: ''
    })
  
  },
  search:function(){
    currentPage = 1
    this.setData({
      pincheData: [],
      nomore: false
    })
    this.getList()
  },
  resetAll:function(){
    currentPage = 1
    this.setData({
      pincheData: [],
      nomore: false
    })
    this.getList()
  },

  getList:function(){
    var that= this
    wx.request({
      url: 'https://api.hgtengfei.cn/publishedInfo/list',
      data:{
        page:(currentPage-1)*pageSize,
        size:pageSize,
        departure: that.data.searchDeparture,
        destination: that.data.searchDestination,
        type:that.data.type
        },
      success:function(rs){
        console.log(rs)
        if(rs.data.length == 0){
          that.setData({
            nomore:true
          })
        }else if(rs.data.length < pageSize){
          that.setData({
            nomore:true,
            pincheData: that.data.pincheData.concat(rs.data)
          })
        }else{
          that.setData({
            pincheData: that.data.pincheData.concat(rs.data)
          })
        }
       
      }
    })
  }
});