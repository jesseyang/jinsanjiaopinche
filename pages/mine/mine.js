// pages/my/mine.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pincheData:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getMyData()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getMyData()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.getMyData()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  getMyData: function () {
    var that = this
    wx.request({
      url: 'https://api.hgtengfei.cn/publishedInfo/mine',
      data: {
        openId:getApp().globalData.userId
      },
      success: function (rs) {
        console.log(rs)
        if (rs.data.length == 0) {
          that.setData({
            nomore: true
          })
        }  else {

          that.setData({
            pincheData: rs.data
          })
        }

      }
    })
  },
  signDepartured: function (event){
    var that = this
    wx.request({
      url: 'https://api.hgtengfei.cn/publishedInfo/signDepartured',
      data: {
       id:event.currentTarget.dataset.itemid
      },
      success: function (data) {
        if (data.data.success) {
          wx.showModal({
            content: data.data.msg,
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              }
            }
          });
        } else {
          wx.showModal({
            content: data.data.msg,
            showCancel: false,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              }
            }
          });
        }
        that.getMyData()

      }
    })
    console.log(event.currentTarget.dataset.itemid);
  }
})