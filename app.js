//app.js
var mydomain = 'https://api.hgtengfei.cn/wechat/user/'
App({
  onLaunch: function () {
    var that = this
    wx.checkSession({
      success: function () {
        console.log('登录了')

        wx.getStorage({
          key: 'userId',
          success: function (res) {
            console.log('get storage success')
            console.log('userid:'+res.data)
            that.globalData.userId = res.data
          },
          fail: function () {
            console.log('没有缓存')
            that.doLogin()
            return
          }
        })
      }, fail: function () {
        console.log('没登录')
        that.doLogin()
        return
      }
    })
    // 查看是否授权
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称
          wx.getUserInfo({
            success: function (res) {
         //     console(res.userInfo)
              console.log(res.userInfo)
              that.globalData.userInfo=res.userInfo
            }
          })
        }
      }
    })
  },
  doLogin: function () {
    console.log('doLogin')
    var that = this;
    wx.login({
      success: function (res) {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        console.log('login success')
        wx.request({
          url: mydomain+'login',
          data:{code:res.code},
          success:function(res){
              console.log(res)
              var userId =  res.data.openid
              wx.setStorage({
                key: 'userId',
                data: userId,
              })

          }
        })
      },
      fail: function () { 
          console.log('login fail')
      }
    })
  },
  globalData: {
    userInfo: null,
    userId:null
  }
})